<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
    // Recuperamos la información de la sesión
    session_start();

    // Y la eliminamos
    session_unset();
    header("Location: login.php");
    ?>
</body>

</html>