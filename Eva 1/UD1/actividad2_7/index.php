<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ejercicio 2.7</title>
</head>

<body>
    <?php
    $error_nombre = false;
    $error_telefono = false;
    $agenda = [];
    if (isset($_POST['nombre']) && isset($_POST['telefono']) && !empty($_POST['nombre']) && !empty($_POST['telefono'])) {
        isset($_POST['agenda']) ? $agenda = unserialize($_POST['agenda']) : "";
        $agenda[] = ["nombre" => $_POST['nombre'], "telefono" => $_POST['telefono']];
    } else {
        if (isset($_POST['agenda'])) {
            $agenda = unserialize($_POST['agenda']);
            empty($_POST['nombre']) ? $error_nombre = true : "";
            empty($_POST['telefono']) ? $error_telefono = true : "";
        }
    }
    ?>
    <table border="1">
        <tr>
            <td>Nombre</td>
            <td>Telefono</td>
        </tr>
        <?php
        foreach ($agenda as $key => $value) {
            echo "<tr>";
            foreach ($value as $k => $v) {
                echo "<td>" . $v . "</td>";
            }
            echo "</td>";
        }
        ?>
    </table>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" required>
            <?php echo $error_nombre ? "Debes completar el campo nombre" : "" ?>
        </div>
        <div>
            <label for="telefono">Telefono</label>
            <input type="text" name="telefono" required>
            <?php echo $error_telefono ? "Debes completar el campo telefono" : "" ?>
        </div>
        <input type="hidden" name="agenda" value=<?php echo serialize($agenda); ?>>
        <button type="submit">Añadir a agenda</button>
    </form>
</body>

</html>