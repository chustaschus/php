<?php

/* 
Imprimir los valores del vector asociativo siguiente usando la estructura de control foreach:
$v[1]=90;
$v[30]=7;
$v['e']=99;
$v['hola']=43;
 */

$v[1]=90;
$v[30]=7;
$v['e']=99;
$v['hola']=43;

foreach($v as $key => $valor){
    printf($key . "-". $valor . "<br/>");
}
