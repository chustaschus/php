<?php

/* 
 * Queremos almacenar en una matriz el número de alumnos con el que cuenta una academia, 
 * ordenados en función del nivel y del idioma que se estudia. 
 * Tendremos 3 filas que representarán al Nivel básico, medio y de 
 * perfeccionamiento y 4 columnas en las que figurarán los idiomas 
 * (0 = Inglés, 
 * 1 = Francés, 
 * 2 = Alemán y 
 * 3 = Ruso). 
 * Se pide realizar la declaración de la matriz y asignarle los valores indicados
 *  en la siguiente imagen a cada elemento de las siguientes maneras 
 * (crea un archivo php por cada una de estas maneras):
a) Con una sintaxis basada exclusivamente en índices, 
 * y mostrar por pantalla los alumnos que existen en cada nivel e idioma.
b) Con una sintaxis basada en el uso anidado de la palabra array,
 *  y mostrar por pantalla los alumnos que existen en cada nivel e idioma.
c) Con una sintaxis que combine el uso de array y el uso de índices,
 *  y mostrar por pantalla los alumnos que existen en cada nivel e idioma. 
 */

echo 'a) Con una sintaxis basada exclusivamente en índices, 
 * y mostrar por pantalla los alumnos que existen en cada nivel e idioma.<br/>';
$num_alums[0][0] = 9;
$num_alums[0][1] = 5;
$num_alums[0][2] = 5;
$num_alums[0][3] = 6;

$num_alums[1][0] = 7;
$num_alums[1][1] = 8;
$num_alums[1][2] = 6;
$num_alums[1][3] = 8;

$num_alums[2][0] = 9;
$num_alums[2][1] = 4;
$num_alums[2][2] = 5;
$num_alums[2][3] = 2;


foreach($num_alums as $valor){
    foreach($valor as $k => $res){
        printf($k . "-" .$res . "        " );
    }
    printf( "<br/>");
}
printf( "<br/>");
echo 'b) Con una sintaxis basada en el uso anidado de la palabra array,
 *  y mostrar por pantalla los alumnos que existen en cada nivel e idioma.<br/>';
$num_alums = array(
    array("Inglés" => 2,"Francés" => 4,"Alemán" => 7,"Ruso" => 9), 
    array("Inglés" =>4,"Francés" => 5,"Alemán" => 6,"Ruso" =>6), 
    array("Inglés" =>2, "Francés" =>7, "Alemán" =>9,"Ruso" =>3));

foreach($num_alums as $valor){
    foreach($valor as $k => $res){
        printf($k . "-" .$res . "        " );
    }
    printf( "<br/>");
}
printf( "<br/>");
echo 'c) Con una sintaxis que combine el uso de array y el uso de índices,
 *  y mostrar por pantalla los alumnos que existen en cada nivel e idioma.<br/>';

$num_alums[0] = array("Inglés" => 2,"Francés" => 4,"Alemán" => 7,"Ruso" => 9);
$num_alums[1] = array("Inglés" =>4,"Francés" => 5,"Alemán" => 6,"Ruso" =>6);
$num_alums[2] = array("Inglés" =>2, "Francés" =>7, "Alemán" =>9,"Ruso" =>3);

foreach($num_alums as $valor){
    foreach($valor as $k => $res){
        printf($k . "-" .$res . "        " );
    }
    printf( "<br/>");
}