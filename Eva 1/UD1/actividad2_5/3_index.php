<?php

/* 
    Almacenar en un vector los 10 primeros número pares. Imprímir cada uno en una línea.
 */
$vec = array();
$num = 0;
for($i = 1; $num < 10; $i++){
    if($i%2 == 0){
        $vec[$i] = $i;
        $num++;      
    }
    
}

foreach ($vec as $variable => $valor){
    printf($variable . "<br/>");
}