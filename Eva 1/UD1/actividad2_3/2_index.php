<?php

/* 
 * 2.- Obtener el mayor de dos números enteros positivos
 */
?>

<html>
    <head>
        <title>Actividad 2_3-2</title>
    </head>
    <body>
        <?php
            $a = 3;
            $b = 2;
            if($a>$b){
                printf("El %d es mayor que %d", $a, $b);
            }else if($b>$a){
                printf("El %d es mayor que %d", $b, $a); 
            }else{
                printf("El %d es igual que %d", $a, $b);
            }
        ?>
    </body>
</html>

