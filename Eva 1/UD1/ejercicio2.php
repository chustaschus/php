<?php
/*  
    Nombre del programa: Ejercicio 2
    Descripcion: Utilizar variables
    Autor: Jesus Periago
*/

/* a. Si un empleado esta casado o no. */

$casado = true;
printf("El valor de la variable casado es $casado");

/* b. Valor maximo no modificable: 999999. */
define maximo = 999999;
printf("El valor de la variable MAXIMO es maximo");

/* c. Dia de la semana */
$diasem = 1;
printf("El valor de la variable diasem es $diasem");


?>