<?php

/* 
5.- Hacer un programa que sume dos variables que almacenan dos números distintos.
 */

?>
<html>
    <head>
        <title>Actividad 2_2-5</title>
    </head>
    <body>
        <?php
            $a = 10;
            $b = 30;
            
            printf("En la variable 'a' tenemos %d y en 'b' tenemos %d", $a, $b);
        ?>
    </body>
</html>
