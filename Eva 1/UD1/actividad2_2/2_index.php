<?php

/* 
2.- Calcular el área de un triangulo cuya fórmula es a=(b*h)/2. 
 */
?>

<html>
    <head>
        <title>Actividad 2_2-2</title>
    </head>
    <body>
        <?php
        
        $b = 5;
        $h = 2;
        $a = ($b*$h)/2;
        printf("El área del triángulo es: %.2f", $a);
        
        
        ?>
    </body>
</html>

