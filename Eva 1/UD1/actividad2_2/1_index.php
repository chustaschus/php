<!DOCTYPE html>
<!--
1.- Dado un salario de un trabajador y un impuesto en porcentaje, mostrar el salario resultante de aplicar el impuesto al salario.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $salario = 1400.52;
        $impuesto = 12.5;       
        printf("El salario es $salario y el impuesto es $impuesto%%, el salario resultante es: %.2f", $salario*($impuesto/100+1));
        ?>
    </body>
</html>
