<?php

/* 
4.- Concatena dos cadenas con el operador punto (.) e imprimir su resultado.
 */
?>
<html>
    <head>
        <title>Actividad 2_2-4</title>
    </head>
    <body>
        <?php
            $a = "Esto es la primera cadena";
            $b = ". Y esto es la segunda cadena";
            
            printf($a . $b);
        ?>
    </body>
</html>

