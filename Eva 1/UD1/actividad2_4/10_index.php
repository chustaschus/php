<?php

/* Escriba una rutina que permita concatenar dos cadenas de caracteres. 
 * Concatenar significa unir dos cadenas. 
 * Ejemplo: cadena1=”Hola”, cadena2=” mundo”, cadenafinal=”Hola mundo”. 
 * La cadena final debe ser retornada por la función y recibida como parámetro (por referencia).
 */

?>

<html>
    <head>
        <title>Actividad 2_4-10</title>
    </head>
    <body>
        <?php
            function conc($a, $b){
                return $a . $b;
            }
            $a = "Es una prueba.";
            $b = "Concatenada";
            printf("Concatenamos \"%s\" con \"%s\", como resultado da \"%s\"", $a, $b, conc($a, $b));
        ?>
    </body>
</html>
