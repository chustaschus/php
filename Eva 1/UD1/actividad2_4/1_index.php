<!DOCTYPE html>
<!--
Escribir una función que calcule el cuadrado de un número y otra función que 
calcule el cubo del mismo número devolviendo cada una el valor resultado 
y mostrándolo después por pantalla.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         function cuadrado($n){
             $num = $n;
             $total = pow($num, 2);
             return $total;
         }
         
         function cubo($n){
             $num = $n;
             $total = pow($num, 3);
             return $total; 
         }

         $num = 4;
         $cuadrado = cuadrado($num);
         $cubo = cubo($num);
         
         printf("El cuadrado de %d es %d <br/>", $num, $cuadrado);
         printf("El cibo de %d es %d <br/>", $num, $cubo);
         
         
        
        ?>
        
        
    </body>
</html>
