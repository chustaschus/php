<?php

/* 
 * Escriba una función que reciba como parámetro una cadena cad y una variable 
 * ch de tipo char. La función devolverá la posición de la primera ocurrencia de
 * ch en card.
 */

?>

<html>
    <head>
        <title>Actividad 2_4-7</title>
    </head>
    <body>
        <?php
        function cadena($cad, $ch){
            $pos = strpos($cad, $ch);
            return $pos;
        }
        $cad = "Leche";
        $ch = "c";
        printf("El char %s en la cadena %s esta por primera vez en la posisción %d", $ch, $cad, cadena($cad, $ch));
        
        ?>
    </body>
</html>

