<?php

/*
 * Escriba una función que reciba una cadena cad y devuelva los caracteres de cad en mayúsculas.
 */

?>

<html>
    <head>
        <title>Actividad 2_4-9</title>
    </head>
    <body>
        <?php
            function may($cad){
                return strtoupper($cad);
            }
            $cad = "Esto es una prueba";
            printf("Transforma la cadena \"%s\" a mayusculas, como resultado sale \"%s\"", $cad, may($cad));
        ?>
    </body>
</html>
