<?php

/* 
Escriba una función que reciba una cadena de caracteres y devuelva su longitud.
 */

?>
<html>
    <head>
        <title>Actividad 2_4-3</title>
    </head>
    <body>
        <?php
        function cadena($c){
            return strlen($c);
        }
        $cadena = "Esto es una prueba";
        $long = cadena($cadena);
        printf("La longitud de la frase %s es %d", $cadena, $long);
        ?>
    </body>
</html>


