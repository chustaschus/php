<?php

/* 
 * Escriba una función que permita verificar si un número n es par o impar.
 */
?>

<html>
    <head>
        <title>Actividad 2_4-6</title>
    </head>
    <body>
        <?php
            function espar($n){
                if(is_numeric($n)){
                    printf("%d  %s", $n, $n%2==0 ? "Es un número par": "Es un número impar");
                }else{
                    printf("%s no es un número", $n);
                }
            }
            
            $num = "s";
            espar($num);
        ?>
    </body>
</html>

