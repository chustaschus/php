<?php

/*
 * Escriba una función lógica que reciba un carácter y retorne si este es una vocal.  
 */
?>

<html>
    <head>
        <title>Actividad 2_4-5</title>
    </head>
    <body>
        <?php
        
        function cadena($c){
            $vocales=array("a", "e", "i", "o", "u");
            $vocal = false;
            if(is_string($c)){
                for($i = 0; $i< sizeof($vocales); $i++){
                    if($vocales[$i] == $c){
                        $vocal = true;
                    }
                }
            }
            return $vocal;
        }
        $ca = "i";
        if(cadena($ca)){
            printf("%s Es una vocal", $ca);    
        }else{
            printf("%s No es una vocal", $ca);
        }
        
        
        ?>
    </body>
</html>

