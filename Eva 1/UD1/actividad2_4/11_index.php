<?php
/*
 * Realizar una función que retorne el cuadrado de un número.
 */
?>

<html>
    <head>
        <title>Actividad 2_4-11</title>
    </head>
    <body>
<?php

function cuadrado($n) {
    if (is_numeric($n)) {
        return pow($n, 2);
    } else {
        return false;
    }
}

$num = 6;
if (cuadrado($num)) {
    printf("El cuadrdado de %d es %d", $num, cuadrado($num));
} else {
    printf("%s no es un número", $num);
}
?>
    </body>
</html>

