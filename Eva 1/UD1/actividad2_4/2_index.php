<?php

/* 
Escriba una función lógica que reciba un carácter y determine si este es un dígito entre ‘0’ a ‘9’.
 */

?>

<html>
    <head>
        <title>Actividad 2_4-2</title>
    </head>
    <body>
        <?php
        function digito($n){
            if(is_numeric($n)){
                if($n>=0 && $n<10){
                    printf("%d Es un número comprendido entre 0 y 9", $n);
                }else{
                    printf("%d no es un número comprendido entre 0 y 9", $n);
                }
            }else{
                printf("%s no es un número", $n);
            }
        }
        
        digito("s");
        ?>
    </body>
</html>

