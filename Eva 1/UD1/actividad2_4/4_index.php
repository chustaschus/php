<?php

/* 
 * Escriba una función que dado dos números enteros a y b, 
 * realice la operación de potencia ab.
 */

?>

<html>
    <head>
        <title>Actividad 2_4-4</title>
    </head>
    <body>
        <?php
        
        function potencia($a, $b){
            return pow($a, $b);
        }
        $a = 2;
        $b = 4;
        printf("La potencia de %d y %d es %d", $a, $b, potencia($a, $b));
        
        ?>
    </body>
</html>
