<?php

/*Escriba una función que reciba una letra y devuelva la correspondiente en 
 * mayúsculas (usar función de se que transforma una minúscula en mayúscula). 
 * Debe validarse si el carácter recibido no es una letra mayúscula, 
 * en cuyo caso se devuelve el mismo carácter.
 */

?>

<html>
    <head>
        <title>Actividad 2_4-8</title>
    </head>
    <body>
        <?php
        function letra($l){
            if(ctype_upper($l)){
                return $l;
            }else{
                return strtoupper($l);
            }
        }
        
        $c = letra("C");
        printf("La letra en mayúsculas es: %s", letra($c));
        ?>
    </body>
</html>
