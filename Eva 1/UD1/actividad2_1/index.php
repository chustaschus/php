<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
    <body>
        <?php
/*  
    Nombre del programa: Ejercicio 2
    Descripcion: Utilizar variables
    Autor: Jesus Periago
*/

/* a. Si un empleado esta casado o no. */
$casado = true;
printf("El valor de la variable casado es %s <br/>", $casado ? "true" : "false" );

/* b. Valor maximo no modificable: 999999. */
define("MAXIMO", 999999);
printf("El valor de la variable MAXIMO es %s<br/>" , MAXIMO);

/* c. Dia de la semana */
$diasem = 1;
printf("El valor de la variable diasem es $diasem <br/>");

/*Dia del ano.*/
$diaanual = 300;
printf("El valor de la variable diaanual es $diaanual <br/>");

/*Sexo: con dos valores posibles 'V' o 'M'*/
$sexo = ["V", "M"];
printf("El valor de la variable sexo es %s <br/>", $sexo[1]);

/*Almacenar el total de una factura*/
$totalfactura = 10350.678;
printf("El valor de la variable totalfactura es %.3f <br/>", $totalfactura);

/*Población mundial del planeta tierra.*/
$poblacion = 6775235741;
printf("El valor de la variable poblacion es %s<br/>", $poblacion);

?>
    </body>
</html>
